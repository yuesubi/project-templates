# The rules for utils.
# - clean
# Requires the definitions of:
# - OUTPUT_DIRECTORY


ifeq ($(OS),Windows_NT)
# Start windows includes
include mk/utils/windows/clean.mk
# End windows includes
else
	UNAMEOS = $(shell uname)
	ifeq ($(UNAMEOS),Linux)
# Start linux includes
include mk/utils/linux/clean.mk
# End linux includes
	else
		@echo "Platform not supported !"
	endif
endif