# The release rule for windows.
# Requires the definitions of:
# - COMPILER
# - FILES_TO_COMPILE
# - OUTPUT_DIRECTORY
# - TARGET_EXECUTABLE
# - FLAGS


.PHONY: release
release:
	@echo "<><><>< COMPILING FOR RELEASE ><><><>"
	-@mkdir "$(OUTPUT_DIRECTORY)"
	"$(COMPILER)" $(FILES_TO_COMPILE) -o "$(OUTPUT_DIRECTORY)/$(TARGET_EXECUTABLE)" $(FLAGS)
	@echo.