# The debug rule for clean.
# Requires the definitions of:
# - OUTPUT_DIRECTORY


.PHONY: clean
clean:
	@echo "<><><>< CLEANING $(OUTPUT_DIRECTORY) ><><><>"
	-rmdir /S /Q "$(OUTPUT_DIRECTORY)"
	@echo.