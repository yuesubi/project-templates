#include "raylib.h"


int main(void)
{
	InitWindow(640, 460, "C raylib template project");

	while (!WindowShouldClose()) {
		BeginDrawing();
			ClearBackground(BLACK);
			DrawFPS(10, 10);
		EndDrawing();
	}

	CloseWindow();

	return 0;
}