# The debug rule for windows.
# Requires the definitions of:
# - COMPILER
# - FILES_TO_COMPILE
# - OUTPUT_DIRECTORY
# - FLAGS


.PHONY: debug
debug:
	@echo "<><><>< COMPILING FOR DEBUG ><><><>"
	-@mkdir "$(OUTPUT_DIRECTORY)"
	"$(COMPILER)" -g $(FILES_TO_COMPILE) -o "$(OUTPUT_DIRECTORY)/debug.exe" $(FLAGS)
	@echo.