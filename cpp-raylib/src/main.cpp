#include "raylib-cpp/Window.hpp"


int main()
{
	raylib::Window win(640, 460, "C++ raylib template project");

	while (!win.ShouldClose()) {
		BeginDrawing();
			ClearBackground(BLACK);
			DrawFPS(10, 10);
		EndDrawing();
	}
}
