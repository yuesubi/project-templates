# The run rule for linux.
# Requires the definitions of:
# - OUTPUT_DIRECTORY
# - TARGET_EXECUTABLE


.PHONY: run
run:
	@echo "<><><>< RUNNING $(OUTPUT_DIRECTORY)/$(TARGET_EXECUTABLE) ><><><>"
	@exec "$(OUTPUT_DIRECTORY)/$(TARGET_EXECUTABLE)"
	@echo ""