# The run rule for windows.
# Requires the definitions of:
# - OUTPUT_DIRECTORY
# - TARGET_EXECUTABLE


.PHONY: run
run:
	@echo "<><><>< RUNNING $(OUTPUT_DIRECTORY)/$(TARGET_EXECUTABLE) ><><><>"
	@"$(OUTPUT_DIRECTORY)/$(TARGET_EXECUTABLE)"
	@echo.