# The rules for an application.
# - release
# - debug
# - run
# Requires the definitions of:
# - COMPILER
# - FILES_TO_COMPILE
# - OUTPUT_DIRECTORY
# - TARGET_EXECUTABLE
# - FLAGS


ifeq ($(OS),Windows_NT)
# Start windows includes
include mk/application/windows/release.mk
include mk/application/windows/debug.mk
include mk/application/windows/run.mk
# End windows includes
else
	UNAMEOS = $(shell uname)
	ifeq ($(UNAMEOS),Linux)
# Start linux includes
include mk/application/linux/release.mk
include mk/application/linux/debug.mk
include mk/application/linux/run.mk
# End linux includes
	else
		@echo "Platform not supported !"
	endif
endif