# The release rule for windows.
# Requires the rules:
# - clean
# - build
# - run_example


.PHONY: release
release: clean build run_example