# Project templates
The purpose of this repository is to have templates for projects, so that it's
easy to start working right away on a project.

## How to use
Copy the project folder you want and change the parametters you want in the `Makefile`.

## Run or build a project
Use the folowing make rules or a combination of them to compile or run your project.

### c
```bash
make release
make run
make debug
make clean
```

### c-custom-lib
```bash
make example=name_of_example.c
make build
make tests
make clean
make release
make build_tests
make run_example
make build_example
```

### c-raylib
```bash
make release
make run
make debug
make clean
```

### cpp
```bash
make release
make run
make debug
make clean
```

### cpp-custom-lib
```bash
make example=name_of_example.cpp
make build
make tests
make clean
make release
make build_tests
make run_example
make build_example
```

### cpp-raylib
```bash
make release
make run
make debug
make clean
```