# The build rule for windows.
# Requires the definitions of:
# - COMPILER
# - ARCHIVER
# - FILES_TO_COMPILE
# - OUTPUT_DIRECTORY
# - LIBRAIRY_NAME
# - FLAGS


OBJECT_FILES := $(FILES_TO_COMPILE:%=$(OUTPUT_DIRECTORY)/%.o)
TARGET_LIBRAIRY := $(OUTPUT_DIRECTORY)/lib$(LIBRAIRY_NAME).a


.PHONY: build
build: $(TARGET_LIBRAIRY)

# Linking step. Making the librairy
$(TARGET_LIBRAIRY): $(OBJECT_FILES)
	@echo "<><><>< LINKING IN $@ ><><><>"
	"$(ARCHIVER)" rcs $@ $<
	@echo.

# Build each C file to its corresponding object file.
$(OUTPUT_DIRECTORY)/%.c.o: %.c
	@echo "<><><>< COMPILING $< TO $@ ><><><>"
	-@mkdir $(dir $@)
	"$(COMPILER)" -c $< -o $@ $(FLAGS)
	@echo.

# Build each C++ file to its corresponding object file.
$(OUTPUT_DIRECTORY)/%.cpp.o: %.cpp
	@echo "<><><>< COMPILING $< TO $@ ><><><>"
	-@mkdir $(dir $@)
	"$(COMPILER)" -c $< -o $@ $(FLAGS)
	@echo.