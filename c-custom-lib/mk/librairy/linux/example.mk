# The example parametter and the run_example and build_example rules for linux.
# Requires the definitions of:
# - COMPILER
# - OUTPUT_DIRECTORY
# - LIBRAIRY_NAME
# - LIBRAIRY_HEADER_FOLDER
# - FLAGS
# - EXAMPLE_FOLDER
# Optional command line arguments
# - example=name_of_example.c


# Get example command line argument
example ?=
TARGET_EXAMPLE := $(OUTPUT_DIRECTORY)/$(EXAMPLE_FOLDER)/$(example)


.PHONY: run_example
run_example: $(example)

.PHONY: $(example)
$(example): build_example
	@echo "<><><>< RUNNING EXAMPLE $(example) ><><><>"
	@exec $(TARGET_EXAMPLE)
	@echo ""

.PHONY: build_example
build_example:
	@echo "<><><>< COMPILING EXAMPLE $(example) ><><><>"
	@mkdir -p $(OUTPUT_DIRECTORY)/$(EXAMPLE_FOLDER)
	"$(COMPILER)" $(EXAMPLE_FOLDER)/$(example) -o $(TARGET_EXAMPLE) $(FLAGS) -I$(LIBRAIRY_HEADER_FOLDER) -L$(OUTPUT_DIRECTORY) -l$(LIBRAIRY_NAME)
	@echo ""