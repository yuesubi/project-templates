# The release rule for linux.
# Requires the rules:
# - clean
# - build
# - run_example


.PHONY: release
release: clean build run_example