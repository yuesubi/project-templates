# The tests and build_tests rules for linux.
# Requires the definitions of:
# - COMPILER
# - TEST_FILE
# - OUTPUT_DIRECTORY
# - LIBRAIRY_NAME
# - LIBRAIRY_HEADER_FOLDER
# - FLAGS
# - TESTS_FOLDER


TARGET_TEST := $(OUTPUT_DIRECTORY)/$(TESTS_FOLDER)/$(TEST_FILE)


.PHONY: tests
tests: build_tests
	@echo "<><><>< RUNNING TESTS ><><><>"
	@exec $(TARGET_TEST)
	@echo ""

.PHONY: build_tests
build_tests:
	@echo "<><><>< COMPILING FOR TESTS ><><><>"
	@mkdir -p $(OUTPUT_DIRECTORY)/$(TESTS_FOLDER)
	"$(COMPILER)" $(TESTS_FOLDER)/$(TEST_FILE) -o $(TARGET_TEST) $(FLAGS) -I$(LIBRAIRY_HEADER_FOLDER) -L$(OUTPUT_DIRECTORY) -l$(LIBRAIRY_NAME)
	@echo ""