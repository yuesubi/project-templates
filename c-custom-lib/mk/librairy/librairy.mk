# The rules for a librairy.
# - build
# - release
# - tests
# - build_tests
# - run_example
# - build_example
# Requires the definitions of:
# - COMPILER
# - ARCHIVER
# - FILES_TO_COMPILE
# - OUTPUT_DIRECTORY
# - LIBRAIRY_NAME
# - FLAGS
# - LIBRAIRY_HEADER_FOLDER
# - EXAMPLE_FOLDER
# - TEST_FILE
# - TESTS_FOLDER
# Requires rules:
# - clean
# Optional command line arguments
# - example=name_of_example.c


ifeq ($(OS),Windows_NT)
# Start windows includes
include mk/librairy/windows/build.mk
include mk/librairy/windows/release.mk
include mk/librairy/windows/example.mk
include mk/librairy/windows/tests.mk
# End windows includes
else
	UNAMEOS = $(shell uname)
	ifeq ($(UNAMEOS),Linux)
# Start linux includes
include mk/librairy/linux/build.mk
include mk/librairy/linux/release.mk
include mk/librairy/linux/example.mk
include mk/librairy/linux/tests.mk
# End linux includes
	else
		@echo "Platform not supported !"
	endif
endif