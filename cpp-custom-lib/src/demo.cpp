#include "demo.hpp"

#include <iostream>


void Hello(const std::string& name)
{
	std::cout << "Hello " << name << "!" << std::endl;
}