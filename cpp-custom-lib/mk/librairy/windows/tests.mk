# The tests and build_tests rule for windows.
# Requires the definitions of:
# - COMPILER
# - TEST_FILE
# - OUTPUT_DIRECTORY
# - LIBRAIRY_NAME
# - LIBRAIRY_HEADER_FOLDER
# - FLAGS
# - TESTS_FOLDER


TARGET_TEST := $(OUTPUT_DIRECTORY)/$(TESTS_FOLDER)/$(TEST_FILE).exe


.PHONY: tests
tests: build_tests
	@echo "<><><>< RUNNING TESTS ><><><>"
	@$(TARGET_TEST)
	@echo.

.PHONY: build_tests
build_tests:
	@echo "<><><>< COMPILING FOR TESTS ><><><>"
	-@mkdir "$(OUTPUT_DIRECTORY)/$(TESTS_FOLDER)"
	"$(COMPILER)" $(TESTS_FOLDER)/$(TEST_FILE) -o $(TARGET_TEST) $(FLAGS) -I$(LIBRAIRY_HEADER_FOLDER) -L$(OUTPUT_DIRECTORY) -l$(LIBRAIRY_NAME)
	@echo.